# transcriptOER

## Installation

### Download

Klone das Repository.

### Voraussetzungen

- Python 3.X (VENV oder (mini)conda empfohlen)
- Python Libraries
  - [Streamlit](https://pypi.org/project/streamlit/)
  - [OpenAI](https://pypi.org/project/openai/)
  - [yt-dlp](https://pypi.org/project/yt-dlp/)
  - [ffmpeg](https://pypi.org/project/ffmpeg/)
  - [pydub](https://pypi.org/project/pydub/)
  - [tiktoken](https://pypi.org/project/tiktoken)
  - [streamlit_js_eval](https://pypi.org/project/streamlit_js_eval/)
- Unix Betriebssystem ob der aktuellen Pfadstruktur
- Installation von ffmpeg (die Python-Version kann nur dann funktionieren, wenn auf dem System ffmpeg selbst ebenso installiert ist.)

### Installation via pip

Allgemein empfiehlt es sich, eine [virtuelle Python-Umgebung](https://docs.python.org/3/library/venv.html) anzulegen.

Via conda: `$ conda create -n transcriptoer python=3.10`

Anschließend lassen sich alle Python-Abhängigkeiten via pip installieren:

`$ pip install -r requirements.txt`

### Konfiguration

Die unteren Pfade in den Zeilen 13 und 14 in main.py anpassen:

- `sys.path.append("~/Documents/GitLab@KIT/isd-td/transcriptoer/web-ui")`
- `PROJECTS_PATH = "~/isd-td/transcriptoer/projects"`

Der erste Pfad ist das Installationsverzeichnis des TranscriptOER, der zweite das Projekteverzeichnis, in dem später deine heruntergeladenen und verarbeiteten Dateien landen werden.

#### Streamlit

Für bessere Performance unter macOS empfiehlt sich das watchdog-Modul:

`$ xcode-select --install`

`$ pip install watchdog`

#### OpenAI

`$ export OPENAI_API_KEY='your-api-key-here'`

Obigen Variablenexport kann man auch direkt in die .bashrc oder .zshrc einbinden, sodass direkt beim Start der Shell der OpenAI-Key verfügbar ist.

## Verwendung

### Start der Weboberfläche

`$ streamlit run web-ui/main.py`

### Fehlermeldung beim ersten Start

Beim ersten Start kommt es im Hauptfenster der Anwendung zu einer Fehlermeldung. Diese hierzu vorerst ignorieren und links in der Sidebar ein neues Projekt anlegen.

### Empfehlungen

Den Zugriff auf die Weboberfläche kann man einfach mit Hilfe eines Reverse-Proxies wie nginx (für den es auch graphische Oberflächen gibt) absichern. So lässt sich über diesen eine Transportverschlüsselung integrieren und eine Authentifizierung ergänzen.

### Oberfläche

#### Projekt anlegen

Nach dem Anlegen eines neuen Projekts lädt sich die Seite neu. Anschließend das neu angelegte Projekt auswählen. Das aktuelle Projekt findet sich immer oben in der Hauptansicht.

#### Transkribieren

Zum Transkribieren einfach einen Link zum Youtube-Video oder aber einer anderen Seite mit eingebetteten Videos einfügen. Hier dürften nahezu alle gängigen Seiten funktionieren, auch solche von Bibliotheken und anderen Plattformen. Das im Hintergrund verwendete Programm »yt-dlp« ist hierbei äußerst umfangreich.

Nach dem Download wird die Tonspur extrahiert und ggf. in mehrere Teile aufgeteilt, sodass diese von der Whisper-API verwendet werden können. Anschließend werden die Transkript-Schnipsel konkateniert und in »transkript.txt« abgespeichert.

#### Zusammenfassen lassen

Die bisherige Version des TranscriptOERs ist relativ simpel. Wir erstellen im ersten Schritt ein Inhaltsverzeichnis und im zweiten eine Zusammenfassung zum jeweiligen Stichpunkt.

Hierbei gibt es für die Zukunft viele Verbesserungspotentiale: Wir könnten einerseits ein Wörterbuch anlegen und für die immergleiche Verwendung von Begriffen sorgen, sodass nicht aus rhetorischen Gründen andere, synonyme Begriffe verwendet werden.

Man kann bei den Prompts erheblich konkreter werden. One- und Few-Shot-Prompting, durch die Beispiele mitgeliefert werden, können sehr gut den Output des Modells beeinflussen, sodass man hier größere Erfolge hat, wenn man einmal ein gutes Beispiel mit angibt.

Ferner wirken die Zusammenfassungen inflationär. Je nach Länge des Transkripts kann es vorkommen, dass Abschnitte wiederholt zusammengefasst werden. Hier wäre eine Nachverarbeitung angebracht.

##### Timecodes

Bei den Timecodes muss ggf. bei Videos unter einer Stunde Länge die Stunde ergänzt werden!

Im Hintergrund geschieht eine simple Verarbeitung: Der Timecode wird abgeschnitten und nur der jeweilige Kapiteltitel weiterverwendet.

Die Timecodes ersetzen das automatische Erstellen eines Inhaltsverzeichnisses.

#### Herunterladen

Die entsprechenden Ordner innerhalb eines Projekts lassen sich löschen, umbenennen und herunterladen. Hierzu wird ad hoc ein zip-Archiv erstellt, das der Browser herunterlädt.

## Zum Projekt

Zur [Website des ISD](https://www.isd.kit.edu/td-projekte_TranscriptOER.php).
