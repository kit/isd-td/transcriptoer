import tiktoken
import streamlit as st

from openai import OpenAI

import re
import os
from datetime import datetime

from tenacity import (
    retry,
    stop_after_attempt,
    wait_random_exponential,
)  # for exponential backoff

# Splits a text into chunks based on the maximum token count of the used GPT model.
#
# Parameters:
# path (str): The path to the file to be read and split into chunks.
# config (dict): A dictionary with configuration parameters, including:
#     - gptmodel (str): The GPT model to be used.
# status (st.status): Status object for status updates.
#
# Returns:
# list: A list of text chunks, each not exceeding the maximum number of tokens.
def tokenizer(path, config: dict, status: st.status):

    tokenbygptmodel = 8192

    gptmodel = config['gptmodel']


    if gptmodel == "gpt-3.5-turbo-16k":
        tokenbygptmodel = 14500
    if gptmodel == "gpt-4":
        tokenbygptmodel = 4096



    file = open(path, "r")
    transcript = file.read()
    status.update(label="Datei geladen.")
    status.write(f"Datei {file.name} erfolgreich eingelesen")
    file.close()

    # Counts the number of tokens in the input text.
    #
    # Parameters:
    # inputtext (str): The text whose tokens are to be counted.
    #
    # Returns:
    # int: Number of tokens.
    def num_tokens(inputtext):
        encoding = tiktoken.get_encoding("cl100k_base")
        encoding = encoding.encode(inputtext)
        count = len(encoding)
        return count


    tokencount = num_tokens(transcript)

    status.write(f"{tokencount} Tokens gezählt.")
    print(tokencount)

    # Checks if the number of tokens in the input text does not exceed a maximum limit.
    #
    # Parameters:
    # inputtext (str): The text whose tokens are to be counted.
    # maxtokens (int, optional): The maximum number of allowed tokens (default is 8192).
    #
    # Returns: 
    # bool: True if the number of tokens is less than or equal to the maximum limit, otherwise False.
    def size_check(inputtext, maxtokens=8192):
        encoding = tiktoken.get_encoding("cl100k_base")
        count = len(encoding.encode(inputtext))
        if count <= maxtokens:
            return True
        else: return False

    # Splits the input text into chunks, each containing a maximum specified number of tokens.
    #
    # Parameters:
    # inputtext (str): The text to be split into chunks.
    # maxtokens (int, optional): The maximum number of allowed tokens per chunk (default is 8192).
    #
    # Returns:
    # list: A list of text chunks, each not exceeding the maximum number of tokens.
    def chunker(inputtext, maxtokens=8192):
        status.update(label="Prüfe Dateigröße")
        status.write(f"Unterteile Text wenn nötig in Chunks kleiner als {maxtokens} Token...")
        # split the inputtext into sentences based on "."
        sentences = inputtext.split(".")

        # initialize an empty list to store the chunks
        chunks = []
        current_chunk = ""

        # iterate over the sentences and form chunks
        for sentence in sentences:
            # check if adding the current sentence to the current chunk exceeds the maxtokens limit
            if size_check(current_chunk + sentence + ".", maxtokens) == False:
                # if yes, add the current chunk to the list of chunks
                chunks.append(current_chunk + ".")
                status.update(label="Unterteile Eingabe in Chunks...")
                # start a new chunk with the current sentence
                current_chunk = sentence
            else:
                # if no, add the current sentence to the current chunk
                current_chunk += sentence + "."

        # add the final chunk to the list of chunks
        if current_chunk:
            chunks.append(current_chunk)

        return chunks

    chunked_transcript = chunker(transcript, tokenbygptmodel)

    sum = 0
    for item in chunked_transcript:
        sum += num_tokens(item)
        print(num_tokens(item))
        print(item)
    status.write(f"Anzahl an Chunks: {len(chunked_transcript)}")
    status.write(f"Gesamttokens nach Unterteilen: {sum}")


    print(f"Anzahl an Chunks: {len(chunked_transcript)}")
    print(f"Gesamttokens: {sum}")
    return chunked_transcript


# Main function for processing a chunked transcript using the OpenAI API and the given configuration.
#
# Parameters:
# chunked_transcript (list): A list of text chunks to be processed.
# config (dict): A dictionary with configuration parameters, including:
#     - systemmessage_core (str): Core message for the system.
#     - instructionmessage_toc (str): Instruction message for the table of contents.
#     - get_summary (bool): Indicates whether a summary should be created.
#     - existing_toc (str): Existing table of contents.
#     - gptmodel (str): The GPT model to be used.
# status (st.status): Status object for status updates.
#
# Returns:
# tuple: A dictionary with the topics and the total processing cost as a string.
def transcriptOER_main(chunked_transcript, config: dict, status: st.status):
    client = OpenAI()
    systemmessage_core = config['systemmessage_core']
    instructionmessage_toc = config['instructionmessage_toc']
    get_summary = config['get_summary']
    existing_toc = config['existing_toc']
    gptmodel = config['gptmodel']

    topicdict = {}
    cost = [[0,0,0,0]] # prompttokencost, completiontokencost, totaltokencost, sumoftotaltokencostsofar

    pattern = r"\d+:\d+:\d+\s+(.*)"


    modelcosts = {
        "gpt-3.5-turbo-16k": {
            "prompttokencost": 0.000003,
            "completiontokencost": 0.000004,
        },
        "gpt-4": {
            "prompttokencost": 0.00006,
            "completiontokencost": 0.00012,
        }

    }


# Converts a list of topics into a comma-separated string.
    def gpttopiclist(topics):
        stringtopiclist = ""
        for topic in topics:
            stringtopiclist = stringtopiclist + topic + ", "

        return stringtopiclist


    # implementation from https://cookbook.openai.com/examples/how_to_handle_rate_limits

    # Retrieves a response from the OpenAI API using the given instructions and content.
    # This function uses a retry decorator to attempt up to 6 retries with exponential backoff in case of errors.
    #
    # Parameters:
    # instruction (str): The instruction for the model.
    # content (str): The content combined with the instruction.
    #
    # Returns:
    # dict: The response from the OpenAI API as a dictionary.
    @retry(wait=wait_random_exponential(min=1, max=60), stop=stop_after_attempt(6))
    def getresponse(instruction, content):

        usermessage = instruction + "\n" + content

        return client.chat.completions.create(
            model=gptmodel,
            messages=[
            {"role": "system", "content": systemmessage_core},
            {"role": "user", "content": usermessage},

            ],
        )

    ###################################################################################

    # Extracts current topics from the response or the existing table of contents.
    def getcurrenttopics(inputresponse):
        topiclist = []
        if existing_toc != """ """:
            topics = existing_toc
            topics = topics.splitlines()
            print(topics)
            # Extract text from yt Timestamps in "0:00:00"
            for index, topic in enumerate(topics):
                matches = re.findall(pattern, topic)

                if matches:
                    print(matches[0])
                    topiclist.append(matches[0])

        else:
            topics = inputresponse
            topics = topics[topics.index("-"):] # cuts everything away until first list point.
            topics = topics.splitlines()

            for index, topic in enumerate(topics):
                #topics[index] = topics[index].split("- ")[1]
                print(topics[index])
                topics[index] = topic.replace('- ', '', 1) # only replaces first occurance in string
                topiclist.append(topics[index])
        return topiclist

    # Core function for processing the transcript using the GPT model
    def transcriptOER(instruction, inputgpt): # Core GPT function - does all the handling of GPT requests

        gptoutput = getresponse(instruction, inputgpt)

        prompttokencost = gptoutput.usage.prompt_tokens * modelcosts[gptmodel]["prompttokencost"]
        completiontokencost = gptoutput.usage.completion_tokens * modelcosts[gptmodel]["prompttokencost"]
        totaltokencost = prompttokencost + completiontokencost
        sumtotaltokencost = cost[-1][-1] + totaltokencost
        costs = [prompttokencost, completiontokencost, totaltokencost, sumtotaltokencost]
        cost.append(costs)
        status.write(f"{round(sumtotaltokencost,2)}$ bisher")
        print(gptoutput.choices[0].message.content)
        return gptoutput.choices[0].message.content

    # Function to summarize a chunk
    def transcriptOERsummarize(topiclist, inputgpt): # Core Summarize function - Summarizes one chunk
        for index, topic in enumerate(topiclist):
            status.update(label=f"{topic}: Job {index+1}/{len(topiclist)}")
            status.write(f"Erstelle Zusammenfassung zu »{topic}« Job {index+1}/{len(topiclist)}")
            getsummary = get_summary.split("{", 1)[0] + topic + get_summary.rsplit("}", 1)[1]
            #status.write(getsummary)
            if topic in topicdict:
                topicdict.update({topic: topicdict[topic] + "\n\n" + str(transcriptOER(getsummary, inputgpt))})
            else:
                topicdict[topic] = str(transcriptOER(getsummary, inputgpt))

    # Main function for processing all chunks
    def transcriptOERall(): # Main function
        for index, chunk in enumerate(chunked_transcript):
            #content = input_generator(chunk)
            content = chunk

            # TODO - getcurrenttopics in Abhängigkeit von existing toc -> wenn existing toc, dann ist die Inhaltverzeichniserstellung via GPT nicht nötig.
            response = transcriptOER(instructionmessage_toc, content)
            status.update(label="Inhaltsverzeichnis erhalten - Aufklappen zum Einsehen!", state="running")
            status.write(f"Themen:\n")

            topics = getcurrenttopics(response)
            status.write(topics)
            status.write(f"{index+1} / {len(chunked_transcript)}")

            transcriptOERsummarize(topics, content)


    status.update(label=f"Starte Anfragen an OpenAI mit Modell {gptmodel}")
    transcriptOERall()
    total_cost = f"{round(cost[-1][-1], 2)}$ Gesamtkosten"
    status.write(total_cost)
    return topicdict, total_cost

# Writes the summaries from topicdict to a Markdown file and saves it at a specified path.
#
# Parameters:
# topicdict (dict): A dictionary with topics as keys and their summaries as values.
# path (str): The path where the Markdown file should be saved.
# config (dict): A dictionary with configuration parameters, including:
#     - gptmodel (str): The GPT model used.
#     - total_cost (str): The total processing cost.
# status (st.status): Status object for status updates.
#
# Returns:
# str: The absolute path of the created Markdown file.
def write_markdown(topicdict, path, config, status: st.status):
    status.update(label="Speichere Zusammenfassung...", state="running", expanded=False)
    path = path+"/zusammenfassungen"
    try:
        os.mkdir(path)
    except:
        status.write("Ordner Zusammenfassungen schon erstellt")

    f = open(f"{path}/zusammenfassung_gesamt_{datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}.md", "w")

    f.write(f"> Zusammenfassung automatisiert mit dem »TranscriptOER« am {datetime.now().strftime('%d.%m.%Y')} um {datetime.now().strftime('%H:%M')}h erstellt.  Verwendetes GPT-Modell: {config['gptmodel']}. {config['total_cost']} \n\n")

    # TODO Überschriften besser managen

    for topic in topicdict:
        toctopic = ""
        if topic[0] != " ":
            toctopic = "# " + topic
            print(toctopic)
        else:
            toctopic = re.sub(r"\s{3}", "## ", topic)
            print(toctopic)
        f.write(toctopic + "\n\n")
        f.write(topicdict[topic] + "\n\n")
        print(topicdict[topic])

    abs_path = f.name
    f.close()
    status.update(label="Gespeichert!", state="complete", expanded=False)

    return abs_path
