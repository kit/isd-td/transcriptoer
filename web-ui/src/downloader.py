from __future__ import unicode_literals

import yt_dlp as youtube_dl
import os
import re
from pydub import AudioSegment
import streamlit as st

from openai import OpenAI

client = OpenAI()
from threading import Thread
from time import sleep


def downloader(link, project_path, statusbar: st.progress):
    class MyLogger(object):
        def debug(self, msg):
            pass

        def warning(self, msg):
            pass

        def error(self, msg):
            print(msg)


    def my_hook(d):
        if d['status'] == "downloading":


            percent_string_colored = d['_percent_str']
            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
            percent_string = ansi_escape.sub('', percent_string_colored)
            percent = percent_string.replace('%','')
            float_value = float(percent.strip())
            speed = 0 if d['speed'] is None else d['speed']
            speed = speed / (1024 * 1024)
            speed = round(speed,2)
            statustext = f"Lade herunter, voraussichtliche Zeit verbleibend: {d['eta']} Sekunden. Aktuelle Geschwindigkeit {speed} MB/s"
            download_part = 0.3 # part of entire progressbar reserved for download
            statusbar.progress((float_value/100)*download_part, text = statustext)
        if d['status'] == 'finished':
            statustext = "Fertig heruntergeladen. Video wird zu reinem Audio umgewandelt. Das kann 1 - 2 Minuten dauern."
            statusbar.progress(30, text = statustext)


    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',}
        ],
        'outtmpl': project_path + '/videos/%(id)s.%(ext)s',
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
        # 'ffmpeg_location': '/path/to/ffmpeg',  # Hier den Pfad zu ffmpeg angegeben, um Fehler zu beheben
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        meta = ydl.extract_info(url=link, download=False)
        result = ydl.download([link])
    print("id should be: " + meta["id"])
    print("abs path should be: " + project_path + "/videos/" +meta["id"] + ".mp3")
    abs_path = project_path + "/videos/" +meta["id"] + ".mp3"
    return abs_path

def splitter(abs_path, statusbar: st.progress):
    filesize = os.stat(abs_path)
    filesize = filesize.st_size / (1024 * 1024)

    currfile = abs_path
    filechunks = 1

    if filesize > 25:
        filechunks = filesize / 24
        filechunks = int(filechunks) + 2

    # we assume that the audio is evenly compressed but an easy and hacky way to ensure that is to just divide the audio in two more chunks than neeeded in the optimal case

    print(str(filesize) + " MB")
    directory = ""


    if filechunks > 1:
        directory = abs_path.rsplit("/", 1)[0] + "/splitted" # gets video folder, assigns directory to save to splitted
        tobecut = AudioSegment.from_mp3(abs_path)
        seconds = tobecut.duration_seconds
        segment = seconds / filechunks
        seconds = segment * 1000 # this is needed as pydub measures time in ms so to get seconds we multiply by 1000

        try:
            os.mkdir(directory) # tries to create ../projects/$project-name$/videos/splitted
        except:
            print(f"Ordner {directory} existiert bereits")


        for index in range(filechunks):
            tobewritten = tobecut[:seconds]
            tobecut = tobecut[seconds:]
            statustext = f"Schreibe Teil {index+1} von {filechunks}"
            statusbar.progress(((index+1) / filechunks)*(30/100) + 0.3, text=statustext)
            tobewritten.export(directory+"/part"+str(index)+".mp3", format="mp3", bitrate="192k")
            statusbar.progress((60/100), text="Audio in passende Abschnitte zergliedert.")
    else:
        statusbar.progress((60/100), text=f"Datei ist mit {round(filesize, 2)} MB kleiner als 25MB und muss nicht weiter verarbeitet werden.")
        print(f"laut splitter ist currfile: {currfile}")
        directory = currfile
        # currfile wird oben einmalig definiert, sodass man abs_path verändern kann, ohne dass hier das Falsche zurückgegeben wird.
    return directory

def whisper_transcriber(project_path, abs_path, statusbar: st.progress):
    audiofiles = []
    transcriptions = []

        # Check if abs_path is a directory
    if os.path.isdir(abs_path):
        for path in os.listdir(abs_path):
            if path.endswith('.mp3'):
                audiofiles.append(abs_path+"/"+path)

        # Check if abs_path is a file
    if os.path.isfile(abs_path):
        if abs_path[-4:] == ".mp3":
            audiofiles.append(abs_path)

    project_path = project_path + "/transkripte"

        # Try to create the directory for transcripts
    try:
        os.mkdir(project_path)
    except:
        print(f"Ordner {project_path} existiert bereits")

    # Function to transcribe audio using OpenAI's Whisper model
    def transcribe_audio(file):
        audiofile = open(file, "rb")
        print(file)
        transcript = client.audio.transcriptions.create(model="whisper-1", file=audiofile, response_format='text')

        # Extract the part number from the filename
        string = str(file[:-4])
        pattern = r"part\d+"
        matches = re.findall(pattern, string)

        # Save the transcript to a file
        savetranscript = open(project_path+"/"+matches[0]+".txt", "w")
        print(transcript)
        print(type(transcript))
        savetranscript.write(transcript)
        savetranscript.close()

    # Function to handle multithreaded transcription
    def multithreaded():
        threads = []
        for index, file in enumerate(audiofiles):
            t = Thread(target=transcribe_audio, args=(file,))
            threads.append(t)
            t.start()
            statustext = f"Übergebe Teil {index+1} von {len(audiofiles)} an Whisper-API - Warte auf Antworten"
            statusbar.progress(0.6 + ((index+1)/len(audiofiles))*(40/101), text=statustext)
            sleep(2)

        for thread in threads:
            thread.join()

    # Function to handle single-threaded transcription
    def singlethreaded():
        for file in audiofiles:
            audiofile = open(file, "rb")
            print(file)
            transcript = client.audio.transcriptions.create(model="whisper-1", file=audiofile, response_format='text')
            savetranscript = open(project_path+"/"+"transkript.txt", "w")
            savetranscript.write(transcript)
            savetranscript.close()

    # Choose between multithreaded and single-threaded based on the number of audio files
    if len(audiofiles) > 1:
        multithreaded()
        transcriptfiles = {}

                # Read all transcript files and store in a dictionary
        for path in os.listdir(project_path):
            if path.endswith('.txt'):
                f = open(project_path + "/" + path, "r")
                transcriptfiles[path] = f.read()
                f.close()

        transcript = ""

                # Concatenate all transcript chunks into a single transcript
        for index, transcript_chunks in enumerate(transcriptfiles):
            transcript = transcript + " " +transcriptfiles["part"+str(index)+".txt"]

        abs_path = project_path + "/transkript.txt"

        # Save the combined transcript to a file
        savetranscript = open(abs_path, "w")
        savetranscript.write(transcript)
        savetranscript.close()
    else:
        singlethreaded()
        abs_path = project_path + "/transkript.txt"

    statustext = "Alle Dateien verarbeitet, gesamtes Transkript bereit zum Herunterladen."
    statusbar.progress(0.99, text=statustext)

    return abs_path
