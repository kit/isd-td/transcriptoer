from openai import OpenAI
client = OpenAI()

response = client.images.generate(
  model="dall-e-3",
  prompt="a person reaching for it's own head, the arm coming from the top and the hand and it's fingers forming the own brain in a photorealistic theme of transcendence",
  size="1024x1024",
  quality="standard",
  n=1,
)

image_url = response.data[0].url
print(image_url)