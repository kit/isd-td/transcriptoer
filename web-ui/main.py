import streamlit as st
from streamlit_js_eval import streamlit_js_eval
import shutil
import tempfile
import zipfile
import time
import os
import sys
import pathlib
from io import StringIO

sys.path.append("/Users/user/Documents/GitLab@KIT/isd-td/transcriptoer/web-ui")
PROJECTS_PATH = "/Users/user/isd-td/transcriptoer/projects"

from src import downloader, splitter, whisper_transcriber
from src import tokenizer
from src import transcriptOER_main
from src import write_markdown

file_path = pathlib.Path(__file__).parent.parent.resolve()

# Set wide layout for more screen realestate
st.set_page_config(layout="wide")

# Initial setup for current project
if 'project' not in st.session_state:
        st.session_state.project = ""


#### Functions

# Lists all non-hidden files in the given directory.
#
# Parameter:
# path (str): The directory path where files are to be listed.
#
# Returns:
# list: A list of filenames that are not directories and do not start with a dot.
def list_files(path):
    print(path)
    available_files = [name for name in os.listdir(path)]
    files_to_be_listed = []
    for file in available_files:
        if os.path.isfile(path+"/"+file) and not os.path.isdir(abs_path+"/"+file) and not os.path.basename(path+"/"+file).startswith('.'):
            files_to_be_listed.append(file)
    return files_to_be_listed


# Sidebar for project selection and creation in a Streamlit app.
with st.sidebar:
    # List current directories in the PROJECTS_PATH, excluding hidden files.
    current_directories = [name for name in os.listdir(PROJECTS_PATH) if name != ".DS_Store"] #if os.path.isdir(name) and name != "__pycache__"]
    print(PROJECTS_PATH)
    # Dropdown to select an existing project or create a new one.
    st.session_state.project = st.selectbox("Bitte Projekt auswählen, andernfalls neues erstellen", current_directories, key='project_selector')

    # Form for creating a new project.
    with st.form("Projekt erstellen"):
        project = st.text_input("Projektname")
        submitted = st.form_submit_button("Projekt erstellen")

    # Handle form submission for project creation.
    if submitted:
        st.session_state.project = project
        if st.session_state.project not in os.listdir(PROJECTS_PATH):
            os.mkdir(PROJECTS_PATH + "/" + st.session_state.project)
            st.success("Projekt erfolgreich erstellt!")
            time.sleep(1)
            streamlit_js_eval(js_expressions="parent.window.location.reload()")
        else:
            st.error("Projekt oder Datei bereits vorhanden! Bitte anderen Namen wählen")

    PROJECT_PATH = (PROJECTS_PATH + "/" + st.session_state.project)
    print(PROJECT_PATH)

# Display the current selected project.
st.write(f"Aktuelles Projekt: {st.session_state.project}")


# Defines the diffrent tabs on the page
# Every tab has his own function

tab1_transcript, tab2_sum, tab3_files = st.tabs(["YT → Transkript", "Transkript → Zusammenfassung in Markdownformat","Dateimanager"])


# Tab called "YT → Transkript"

# Here you can give a youtube-link,
# the content will be convert to a txt-file,
# this one you can download
# Many other sites supported by yt-dlp will work as well! Nearly any site with an embedded video.

with tab1_transcript:
    st.write("Vom Video zum Transkript")
    col1, col2, col3 = st.columns(3)

    with col1:
        format = st.selectbox("Eingabeformat", ["Youtube-Link"])

        if format == "Youtube-Link":
            with st.form("Youtube-Transkript"):
                link = st.text_input("Youtube-Link [viele weitere Plattformen mit eingebetteten Videos werden ebenfalls unterstützt. Nur keine Playlists.]")
                submitted_tab1 = st.form_submit_button("Transkript erstellen")
            if submitted_tab1:

                statusbar = st.progress(0, text="Starte Download...")
                abs_path = downloader(link, PROJECTS_PATH + "/" + st.session_state.project, statusbar)
                print(f"downloader returns: {abs_path}")
                abs_path = splitter(abs_path, statusbar)
                #abs_path = "/Users/user/Documents/GitLab@KIT/transkriptoer/dev/projects/testing/videos/splitted"
                print(f"splitter returns: {abs_path}")
                abs_path = whisper_transcriber(PROJECTS_PATH + "/" + st.session_state.project, abs_path, statusbar)
                print(f"whisper returns: {abs_path}")
                #abs_path = "/Users/user/Documents/GitLab@KIT/transkriptoer/dev/projects/testing/transkripte/transkript.txt"
                transcription_download = ""
                with open(abs_path, 'r') as download_file:
                    transcription_download = download_file.read()
                statusbar.empty()
                st.download_button('Transkript herunterladen', file_name="transkript.txt", data=transcription_download)




# Tab called "Transkript → Zusammenfassung in Markdownformat"

# Here you can upload a txt-file,
# the content will be summarized in a new txt-file,
# this one you can download
with tab2_sum:
    st.write("Vom Transkript zur Zusammenfassung und dem Markdown-Dokument")
    abs_path = PROJECT_PATH + "/transkripte"

    try:
        list_files(abs_path)
    except:
        print("Ordner noch nicht vorhanden, erstelle /transkripte im Projekt")
        try:
            os.mkdir(abs_path)
        except:
            st.error("Unerwarteter Fehler!")
    print(list_files(abs_path))

    col1, col2, col3 = st.columns([2, 2, 2])
    current_files = list_files(abs_path)

    with col1:
        pass
    with col2:
        file_select = st.selectbox("Verfügbare Dateien", current_files, placeholder="Bitte auswählen")
        new_document = st.file_uploader("Neues Textdokument (.txt) hochladen", type='txt')
        if new_document is not None:
            string_data = StringIO(new_document.getvalue().decode("utf-8")).read()
            file_name = new_document.name


            try:
                with open(abs_path+"/"+file_name, "w") as savetranscript:
                    savetranscript.write(string_data)

                current_files.append(file_name)
            except:
                st.error("Fehler beim Schreiben")

    with col3:
        pass

    if 'transcriptOER_config' not in st.session_state:
        st.session_state.transcriptOER_config = {}

    col1, col2 = st.columns([3, 1])


    with col1:
        st.session_state.transcriptOER_config['systemmessage_core'] = st.text_area("System-Message", value="Du bist ein System namens »TranscriptOER«. Deine Aufgabe ist es, Inhalte aus Vorlesungstranskripten für Studierende auf Bachelorniveau zusammenzufassen. Verwende hierzu ausschließlich Informationen aus dem Transkript. Verwende Markdownsyntax und schreibe im unpersönlichen Passiv.")
        st.session_state.transcriptOER_config['instructionmessage_toc'] = st.text_area("Anweisung für das Inhaltsverzeichnis / die Übersicht", value="Erstelle zum vorliegenden Transkript ein detailliertes Inhaltsverzeichnis zur Themenübersicht. Gib es als unnummerierte Liste ohne Einrückungsebenen zurück. Ergänze nichts weiteres in deiner Ausgabe.\n #### BEGINN BEISPIEL ####\n- Einführung\n- Rückblick\n- Konzept A\n- Konzept B\n- Konzept C\n- Fazit\n #### ENDE BEISPIEL ####")
        st.session_state.transcriptOER_config['get_summary'] = st.text_area("Anweisung zum Zusammenfassen des aktuellen Themas, referenziert durch {topic}", value="Fasse das Thema »{topic}« anhand des vorliegenden Transkripts zusammen. Gliedere die Zusammenfassung so, dass sie gut zum Lernen geeignet ist und verwende hierbei Markdownsyntax.")
        st.session_state.transcriptOER_config['existing_toc'] = st.text_area("Inhaltsverzeichnis im Youtube-Timecode-Format, wenn vorhanden:\n\n ```0:00:00 Thema```", value=""" """)
    with col2:
        with st.form("TranscriptOER"):
            st.session_state.transcriptOER_config['gptmodel'] = st.radio("GPT-Modell (4 bietet nur geringe Qualitätsverbesserungen für diesen Zweck)", options=['gpt-3.5-turbo-16k', 'gpt-4'])
            submitted_tab2 = st.form_submit_button("Prozess starten")
        if submitted_tab2:
            with st.status("Öffne Datei...") as status:
                abs_path = PROJECT_PATH + "/transkripte/" + file_select
                chunks = tokenizer(abs_path, st.session_state.transcriptOER_config, status)
                topics, st.session_state.transcriptOER_config['total_cost'] = transcriptOER_main(chunks, st.session_state.transcriptOER_config, status)
                output = write_markdown(topics, PROJECT_PATH, st.session_state.transcriptOER_config, status)
                print(output)
            summary = ""
            output_filename=output.rsplit("/",1)[1]
            try:
                with open(output, 'r') as download_file:
                    summarydownload = download_file.read()
                    st.download_button('Zusammenfassung herunterladen', file_name=output_filename, data=summarydownload)
            except:
                st.error("Fehler beim Öffnen der Zusammenfassung...")


# Tab called "File and Directory Manager"
# Manages the edited files
with tab3_files:
    abs_path = PROJECT_PATH
    st.write("File and Directory Manager")

    # Function to list all files and directories
    def list_all_items(path):
        items = os.listdir(path)
        return sorted(items, key=lambda x: (os.path.isfile(os.path.join(path, x)), x.lower()))

    # Function to create a zip file from directory or file for download
    def make_zipfile(output_filename, source_dir):
        with tempfile.TemporaryDirectory() as tmpdirname:
            tmpzip = os.path.join(tmpdirname, output_filename)
            with zipfile.ZipFile(tmpzip, 'w', zipfile.ZIP_DEFLATED) as zipf:
                if os.path.isdir(source_dir):
                    for root, dirs, files in os.walk(source_dir):
                        for file in files:
                            file_path = os.path.join(root, file)
                            zipf.write(file_path, arcname=os.path.relpath(file_path, os.path.dirname(source_dir)))
                else:
                    zipf.write(source_dir, arcname=os.path.basename(source_dir))
            with open(tmpzip, 'rb') as f:
                data = f.read()
        return data

    # A helper function to download files or directories
    def download_item(item_path, item_name):
        if os.path.isdir(item_path):
            zip_name = f"{item_name}.zip"
            zip_data = make_zipfile(zip_name, item_path)
            st.download_button(label=f"Download {item_name}", data=zip_data, file_name=zip_name, mime='application/zip')
        elif os.path.isfile(item_path):
            with open(item_path, 'rb') as f:
                st.download_button(label=f"Download {item_name}", data=f, file_name=item_name, mime='application/octet-stream')

    items = list_all_items(abs_path)
    for item in items:
        item_path = os.path.join(abs_path, item)
        is_dir = os.path.isdir(item_path)
        col1, col2, col3, col4 = st.columns(4)
        with col1:
            st.write(item)
        with col2:
            if st.button(f"Rename {item}", key=f"rename_{item}"):
                new_name = st.text_input("New name for " + item, key=f"new_name_{item}")
                if new_name:
                    try:
                        new_item_path = os.path.join(abs_path, new_name)
                        os.rename(item_path, new_item_path)
                        st.experimental_rerun()
                    except Exception as e:
                        st.error(f"Error renaming: {e}")
        with col3:
            if st.button(f"Download {item}"):
                download_item(item_path, item)
        with col4:
            if st.button(f"Delete {item}", key=f"delete_{item}"):
                try:
                    if is_dir:
                        shutil.rmtree(item_path)
                    else:
                        os.remove(item_path)
                    st.experimental_rerun()
                except Exception as e:
                    st.error(f"Error deleting: {e}")
